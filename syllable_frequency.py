#!/usr/bin/env python

import sys
import os
import subprocess
from collections import defaultdict
import operator
from pprint import pprint


def system(cmd):
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    return proc.communicate()[0]


def main():
    txt = sys.argv[1]

    stdout = system(["./word_frequency.sh", txt])

    syllable_count = defaultdict(int)

    for line in stdout.splitlines():
        occurences, word = line.split(',')
        syllables = system(['./syllables.sh', word])
        syllable_count[syllables] += int(occurences)
        #print word, syllables

    syllable_count_sorted = sorted(syllable_count.iteritems(), key=operator.itemgetter(1), reverse=True)
    pprint(syllable_count_sorted)

if __name__ == '__main__':
    main()
