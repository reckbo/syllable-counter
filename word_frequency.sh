#!/bin/bash

tr 'A-Z' 'a-z' <$1 \
    | tr -cs 'a-z' '\n' \
    | sort \
    | uniq -c \
    | sed 's/^[ \t]*//' \
    | sed 's/[ ]/,/'
