#!/bin/bash

word=$(tr 'a-z' 'A-Z' <<< $1)
egrep "^$word," dict | cut -d, -f2
