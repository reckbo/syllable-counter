Computes the frequency of word syllables in a text document. 
Right now it is slow!  And doesn't recognize contractions
like "doesn't".

Todo:

* make faster
* make it recognize contractions

Dependencies:

* bash
* python

Run
---

    ./syllables.sh <word>
    ./syllable_frequency.py <text_document>


